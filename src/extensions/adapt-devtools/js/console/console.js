/*
Portions of this module (adapt-devtools/js/utils) are subject to the following license:

Software License Agreement (BSD License)

Copyright (c) 2007, Parakey Inc.
All rights reserved.

Redistribution and use of this software in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

* Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

* Neither the name of Parakey Inc. nor the names of its
  contributors may be used to endorse or promote products
  derived from this software without specific prior
  written permission of Parakey Inc.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

define(function(require) {

	var Adapt = require('coreJS/adapt');
	var Router = require('coreJS/router');
	var Utils = require('../utils');
	var Reps = require('./reps');

	var ConsoleView = Backbone.View.extend({

		className:'devtools-console',

		events: {
			'click .toggle-console':'onToggle'
		},

		initialize:function() {
			this.output = [];
			this.outputPos = 0;

			this.render();

			window.addEventListener('error', _.bind(this.onError, this));

			this.listenTo(Adapt, 'device:resize', this.onDeviceResize);
			this.listenToOnce(Adapt, 'adapt:start', this.onAdaptStart);
		},

		render:function() {
			var template = Handlebars.templates['devtoolsConsole'];
			this.$el.html(template({output:this.output}));
			$('body').append(this.$el);
			this.outputPos = this.output.length;
			this.refreshDimensions();
		},

		update:function() {
			return;
			if (this.outputPos == 0) {
				this.render();
			} else {
				this.append();
			}
		},

		append:function() {
			for (var i = this.outputPos, count = this.output.length; i < count; i++) {
				var output = this.output[i];

				if (output._type == 'stackTrace') {
					this.addStackTrace(output._data);
				} else if (output._type == 'stackTrace') {
					this.addError(output._data);
				}
			}

			this.outputPos = this.output.length;
		},

		addStackTrace:function(data) {
			this.$('.output').append(Handlebars.partials['consoleStackTrace'](data));
		},

		addError:function(data) {
			this.$('.output').append(Handlebars.partials['consoleError'](data));
		},

		onError:function(e) {
			console.log(e);

			if (e.error) {
				var message = e.message;
				var stack = e.error.stack;
				var itemised = stack.split('\n');
				var calls = [];

				_.each(itemised, function(item) {
					if (_.isEmpty(item)) return;

					var pathDelimiterIndex = item.indexOf('@');
					var fileDelimiterIndex = item.lastIndexOf('/');
					var fcall = item.substring(0, pathDelimiterIndex);
					var pname = item.substring(pathDelimiterIndex + 1, fileDelimiterIndex);
					var fname = item.substring(fileDelimiterIndex + 1);

					if (_.isEmpty(fcall)) return;

					calls.push({
						fcall:fcall,
						pname:pname,
						fname:fname
					});
				});

				this.output.push({_type:'stackTrace', _data:{message:message, calls:calls}});
			} else {
				this.output.push({_type:'error', _data:{message:e.message, fname:e.filename, colno:e.colno, lineno:e.lineno}});
			}

			this.update();
		},

		close:function(animate) {
			var props = {
				'easing':'easeIn'
			};

			if (animate === false) props.duration = 0;

			this.$('.output').slideUp();

			this.$el.removeClass('open');
		},

		open:function(animate) {
			var props = {
				'easing':'easeIn'
			};

			if (animate === false) props.duration = 0;

			this.$('.output').slideDown();

			this.$el.addClass('open');
		},

		clear:function() {
			
		},

		logFormatted:function(objects, context, className, noThrottle, sourceLink) {
			this.logRow(this.appendFormatted, objects, context, className, null, sourceLink, noThrottle);
		},

		logRow:function(appender, objects, context, className, rep, sourceLink, noThrottle, noRow) {
			this.append2(appender, objects, className, rep, sourceLink, noRow);
		},

		append2: function(appender, objects, className, rep, sourceLink, noRow) {
	        var container = this.getTopContainer();

	        if (noRow)
	        {
	            appender.apply(this, [objects]);
	        }
	        else
	        {

	            var row = this.createRow("logRow", className);
	            appender.apply(this, [objects, row, rep]);

	            // CS TODO: check this out (though I think this is defunct in FBL)
	            /*if (sourceLink)
	                FirebugReps.SourceLink.tag.append({object: sourceLink}, row);*/

	            container.append(row);

	            // CS TODO: consider
	            /*this.filterLogRow(row, this.wasScrolledToBottom);

	            if (this.wasScrolledToBottom)
	                scrollToBottom(this.panelNode);*/

	            return row;
	        }
	    },

	    appendObject: function(object, row, rep) {
	        if (!rep)
	            rep = Reps.getRep(object);
	        window.originalConsole.log('appendObject');
	        return rep.append({object: object}, row);
	    },

	    appendFormatted: function(objects, row, rep) {
	        if (!objects || !objects.length)
	            return;

	        function logText(text, row)
	        {
	            var node = row[0].ownerDocument.createTextNode(text);
	            window.originalConsole.log('appendFormatted:logText');
	            row.append(node);
	        }

	        var format = objects[0];
	        var objIndex = 0;

	        if (typeof(format) != "string")
	        {
	            format = "";
	            objIndex = -1;
	        }
	        else  // a string
	        {
	            if (objects.length === 1) // then we have only a string...
	            {
	                if (format.length < 1) { // ...and it has no characters.
	                    logText("(an empty string)", row);
	                    return;
	                }
	            }
	        }

	        var parts = this.parseFormat(format);
	        var trialIndex = objIndex;
	        for (var i= 0; i < parts.length; i++)
	        {
	            var part = parts[i];
	            if (part && typeof(part) == "object")
	            {
	                if (++trialIndex > objects.length)  // then too few parameters for format, assume unformatted.
	                {
	                    format = "";
	                    objIndex = -1;
	                    parts.length = 0;
	                    break;
	                }
	            }

	        }
	        for (var i = 0; i < parts.length; ++i)
	        {
	            var part = parts[i];
	            if (part && typeof(part) == "object")
	            {
	                var object = objects[++objIndex];
	                if (typeof(object) != "undefined")
	                    this.appendObject(object, row, part.rep);
	                else
	                    this.appendObject(part.type, row, Reps.Text);
	            }
	            else
	                Reps.Text.append({object: part}, row);
	        }

	        for (var i = objIndex+1; i < objects.length; ++i)
	        {
	            logText(" ", row);
	            var object = objects[i];
	            if (typeof(object) == "string")
	                Reps.Text.append({object: object}, row);
	            else
	                this.appendObject(object, row);
	        }
	    },

	    parseFormat: function(format)
		{
		    var parts = [];
		    if (format.length <= 0)
		        return parts;

		    var reg = /((^%|.%)(\d+)?(\.)([a-zA-Z]))|((^%|.%)([a-zA-Z]))/;
		    for (var m = reg.exec(format); m; m = reg.exec(format))
		    {
		        if (m[0].substr(0, 2) == "%%")
		        {
		            parts.push(format.substr(0, m.index));
		            parts.push(m[0].substr(1));
		        }
		        else
		        {
		            var type = m[8] ? m[8] : m[5];
		            var precision = m[3] ? parseInt(m[3]) : (m[4] == "." ? -1 : 0);

		            var rep = null;
		            switch (type)
		            {
		                case "s":
		                    rep = Reps.Text;
		                    break;
		                case "f":
		                case "i":
		                case "d":
		                    rep = Reps.Number;
		                    break;
		                case "o":
		                    rep = null;
		                    break;
		            }

		            parts.push(format.substr(0, m[0][0] == "%" ? m.index : m.index+1));
		            parts.push({rep: rep, precision: precision, type: ("%" + type)});
		        }

		        format = format.substr(m.index+m[0].length);
		    }

		    parts.push(format);
		    return parts;
		},

	    createRow: function(rowName, className) {
	    	return $('<div/>').addClass(rowName + (className ? " " + rowName + "-" + className : ""));
	    },

	    getTopContainer: function() {
	    	//TODO: implement groups
	        if (this.groups && this.groups.length)
	            return this.groups[this.groups.length-1];
	        else
	            return this.$('.output');
	    },

	    refreshDimensions:function() {
	    	var winHeight = $(window).height();
	    	var navHeight = $('.navigation').height() || 0;
	    	var toolbarHeight = this.$('.toolbar').height() || 0;
	    	
	    	this.$('.output-container').css('max-height', winHeight - navHeight - toolbarHeight);
	    },

		onToggle:function() {
			var isOpen = this.$el.hasClass('open');

			if (isOpen) {
				this.close();
			} else {
				this.open();
			}
		},

		onDeviceResize: function() {
			this.refreshDimensions();
		},

		onAdaptStart: function() {
			this.refreshDimensions();
		}
	});

	function init() {
		Adapt.devtools.console = new ConsoleView();
	}

	/*Adapt.once('adapt:initialize devtools:enable', function() {
		if (!Adapt.devtools.get('_isEnabled')) return;

		init();
	});*/

	init();
});

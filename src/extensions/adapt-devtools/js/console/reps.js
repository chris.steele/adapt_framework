define([
	'coreJS/adapt',
	'./utils'
], function(Adapt, Utils) {
	// for Obj we need propIterator to create the 'title' view
	// only when clicked should the body be created
	// implement supportsObject

	var Reps = {
		reps:[],

		registerRep:function(name, rep) {
			this[name] = rep;
			this.reps.push(rep);
		},

		getRep:function(object) {
	    	var type = typeof object;
	        
	        for (var i = 0; i < this.reps.length; ++i)
	        {
	            var rep = this.reps[i];
	            try
	            {
	                if (rep.supportsObject(object, type))
	                {
	                    /*if (FBTrace.DBG_DOM)
	                        FBTrace.sysout("getRep type: "+type+" object: "+object, rep);*/
	                    return rep;
	                }
	            }
	            catch (exc)
	            {
	                /*if (FBTrace.DBG_ERRORS)
	                {
	                    FBTrace.sysout("firebug.getRep FAILS: ", exc.message || exc);
	                    FBTrace.sysout("firebug.getRep reps["+i+"/"+reps.length+"]: Rep="+reps[i].className);
	                    // TODO: xxxpedro add trace to FBTrace logs like in Firebug
	                    //firebug.trace();
	                }*/
	            }
	        }

	        return (type == 'function') ? Func : Obj;
	    }
	};

	var Rep = {
		supportsObject: function(object, type) {
	        return false;
	    },

		getTitle: function(object) {
	        var label = Utils.safeToString(object);

	        var re = /\[object (.*?)\]/;
	        var m = re.exec(label);
	        
	        ///return m ? m[1] : label;
	        
	        // if the label is in the "[object TYPE]" format return its type
	        if (m)
	        {
	            return m[1];
	        }
	        // if it is IE we need to handle some special cases
	        else if (
	                // safeToString() fails to recognize some objects in IE
	                Utils.isIE && 
	                // safeToString() returns "[object]" for some objects like window.Image 
	                (label == "[object]" || 
	                // safeToString() returns undefined for some objects like window.clientInformation 
	                typeof object == "object" && typeof label == "undefined")
	            )
	        {
	            return "Object";
	        }
	        else
	        {
	            return label;
	        }
	    }
	};

	// Number
	var Num = _.extend({}, Rep, {
	    append:function(object, row) {
	    	window.originalConsole.log('Num:append', object, row);

	    	row.append(Handlebars.templates['consoleNum'](object));
		},

		supportsObject:function(object, type) {
			return type == "boolean" || type == "number";
		}
	});

	// Text
	var Text = _.extend({}, Rep, {
	    append:function(object, row) {
	    	window.originalConsole.log('Text:append', object, row);

	    	row.append(Handlebars.templates['consoleText'](object));
		},

		supportsObject:function(object, type) {
			return false;
		}
	});

	// functions
	var Func = _.extend({}, Rep, {
		append:function(object, row) {
			window.originalConsole.log('Func:append', object, row);
		},

		supportsObject:function(object, type) {
			Utils.isFunction(object);
		}
	});

	// objects
	var Obj = _.extend({}, Rep, {
		append:function(args, row) {
			window.originalConsole.log('Obj:append', object, row);

			var object = args.object;
			var maxLength = 55; // default max length for long representation
        
	        if (!object)
	            return;

	        var props = [];
	        var length = 0;
	        
	        var numProperties = 0;
	        var numPropertiesShown = 0;
	        var maxLengthReached = false;
	        
	        var lib = this;
	        
	        /*var propRepsMap = 
	        {
	            "boolean": this.propNumberTag,
	            "number": this.propNumberTag,
	            "string": this.propStringTag,
	            "object": this.propObjectTag
	        };*/

	        try
	        {
	            var title = this.getTitle(object);
	            length += title.length;

	            for (var name in object)
	            {
	                var value;
	                try
	                {
	                    value = object[name];
	                }
	                catch (exc)
	                {
	                    continue;
	                }
	                
	                var type = typeof(value);
	                if (type == "boolean" || 
	                    type == "number" || 
	                    (type == "string" && value) || 
	                    (type == "object" && value && value.toString))
	                {
	                    //var tag = propRepsMap[type];
	                    
	                    var value = (type == "object") ?
	                        Reps.getRep(value).getTitle(value) :
	                        value + "";
	                        
	                    length += name.length + value.length + 4;
	                    
	                    if (length <= maxLength)
	                    {
	                        props.push({
	                            type: type, 
	                            name: name, 
	                            object: value, 
	                            equal: "=", 
	                            delim: ", "
	                        });
	                        
	                        numPropertiesShown++;
	                    }
	                    else
	                        maxLengthReached = true;

	                }
	                
	                numProperties++;
	                
	                if (maxLengthReached && numProperties > numPropertiesShown)
	                    break;
	            }
	            
	            if (numProperties > numPropertiesShown)
	            {
	                props.push({
	                    object: "...", //xxxHonza localization
	                    /*tag: FirebugReps.Caption.tag,*/
	                    name: "",
	                    equal:"",
	                    delim:""
	                });
	            }
	            else if (props.length > 0)
	            {
	                props[props.length-1].delim = '';
	            }
	        }
	        catch (exc)
	        {
	            // Sometimes we get exceptions when trying to read from certain objects, like
	            // StorageList, but don't let that gum up the works
	            // XXXjjb also History.previous fails because object is a web-page object which does not have
	            // permission to read the history
	        }

	        row.append(Handlebars.templates['consoleObject']({title:title, props:props}));

	        //return props;
		},

		supportsObject:function(object, type) {
			return true;
		}
	});

	Reps.registerRep('Number', Num);
	Reps.registerRep('Text', Text);
	Reps.registerRep('Func', Func);
	Reps.registerRep('Obj', Obj);

	return Reps;
});
define([
], function(Adapt) {

	var userAgent = navigator.userAgent.toLowerCase();
	var reFunction = /^\s*function(\s+[\w_$][\w\d_$]*)?\s*\(/;

	return {
		isFirefox: /firefox/.test(userAgent),
		isOpera: /opera/.test(userAgent),
		isSafari: /webkit/.test(userAgent),
		isIE: /msie/.test(userAgent) && !/opera/.test(userAgent),
		isIE6: /msie 6/i.test(navigator.appVersion),
		browserVersion: (userAgent.match( /.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/ ) || [0,'0'])[1],
		isIElt8: this.isIE && (this.browserVersion-0 < 8),

		safeToString:function(ob) {    
		    if (this.isIE)
		    {
		        try
		        {
		            // FIXME: xxxpedro this is failing in IE for the global "external" object
		            return ob + "";
		        }
		        catch(E)
		        {
		            /*FBTrace.sysout("Lib.safeToString() failed for ", ob);*/
		            return "";
		        }
		    }
		    
		    try
		    {
		        if (ob && "toString" in ob && typeof ob.toString == "function")
		            return ob.toString();
		    }
		    catch (exc)
		    {
		        // xxxpedro it is not safe to use ob+""?
		        return ob + "";
		        ///return "[an object with no toString() function]";
		    }
		},

		isFunction:function(object) {
		    if (!object) return false;

		    try
		    {
		        return toString.call(object) === "[object Function]" || 
                this.isIE && typeof object != "string" && reFunction.test(""+object);
		    }
		    catch (E)
		    {
		        /*FBTrace.sysout("Lib.isFunction() failed for ", object);*/
		        return false;
		    }
		}
	}
});
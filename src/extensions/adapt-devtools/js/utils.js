define(function(require) {

	var Adapt = require('coreJS/adapt');
	var AdaptModel = require('coreModels/adaptModel');
	var QuestionView = require('coreViews/questionView');

	var mouseTarget = null;

	function onMouseDown(e) {
		if (e.which == 1) mouseTarget = e.target;
	}

	function onMouseUp(e) {
		if (e.which == 1) mouseTarget = null;
	}

	function onKeypress(e) {
		var char = String.fromCharCode(e.which).toLowerCase();

		if (mouseTarget) {
			if (char == 'm') {
				var model = Utils.getModelForElement(mouseTarget);

				if (model) {
					id = model.get('_id').replace(/-/g, '');
					window[id] = model;
					console.log('devtools: add property window.'+id+':');
					console.log(model.attributes);
				}
			}
		}
	}

	function getAdaptCoreVersion() {
		try {
			if (Adapt.build && Adapt.build.has('package')) return Adapt.build.get('package').version || ">=v3.0.0";
			if (typeof AdaptModel.prototype.setCompletionStatus == 'function') return ">=v2.0.10";
			if (typeof AdaptModel.prototype.checkLocking == 'function') return "v2.0.9";
			if (typeof Adapt.checkingCompletion == 'function') return "v2.0.8";
			if (typeof AdaptModel.prototype.getParents == 'function') return "v2.0.7";
			if ($.a11y && $.a11y.options.hasOwnProperty('isIOSFixesEnabled')) return "v2.0.5-v2.0.6";
			if (Adapt instanceof Backbone.Model) return "v2.0.4";
			if (typeof QuestionView.prototype.recordInteraction == 'function') return "v2.0.2-v2.0.3";
			if (typeof Adapt.findById == 'function') return "v2.0.0-v2.0.1";
			return "v1.x";
		}
		catch (e) {
			return 'unknown version';
		}
	}

	Utils = {
		initialize:function() {
			var evalError = '___devtools_evaluation_error___';
			// Create a new "black-box" eval() method that runs in the global namespace
		    // of the context window, without exposing the local variables declared
		    // by the function that calls it
		    this.eval = window.eval("new Function('" +
		            "try{ return window.eval.apply(window,arguments) }catch(E){ E."+evalError+"=true; return E }" +
			"')");
		},

		getModelForElement:function(element) {
			var $target = $(element);

			if ($target.length == 0) return false;

			var id = $target.parents('[data-adapt-id]').data('adapt-id');

			return !id ? false : Adapt.findById(id);
		},

		stripNewLines:function(value)
		{
		    return typeof(value) == "string" ? value.replace(/[\r\n]/g, " ") : value;
		},

		// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	    // Evalutation Method
	    
	    /**
	     * Evaluates an expression in the current context window.
	     * 
	     * @param {String}   expr           expression to be evaluated
	     * 
	     * @param {String}   context        string indicating the global location
	     *                                  of the object that will be used as the
	     *                                  context. The context is referred in
	     *                                  the expression as the "this" keyword.
	     *                                  If no context is informed, the "window"
	     *                                  context is used.
	     *                                  
	     * @param {String}   api            string indicating the global location
	     *                                  of the object that will be used as the
	     *                                  api of the evaluation.
	     *                                  
	     * @param {Function} errorHandler(message) error handler to be called
	     *                                         if the evaluation fails.
	     */
	    evaluate: function(expr, context, api, errorHandler)
	    {
	        // Need to remove line breaks otherwise only the first line will be executed
	        expr = this.stripNewLines(expr);
	        
	        // the default context is the "window" object. It can be any string that represents
	        // a global accessible element as: "my.namespaced.object"
	        context = context || "window";
	        
	        var cmd,
	            result;
	        
	        // if the context is the "window" object, we don't need a closure
	        if (context == "window")
	        {
	            // try first the expression wrapped in parenthesis (so we can capture 
	            // object literal expressions like "{}" and "{some:1,props:2}")
	            cmd = api ?
	                "with("+api+"){ ("+expr+") }" :
	                "(" + expr + ")";
	            
	            result = this.eval(cmd);
	            
	            // if it results in error, then try it without parenthesis 
	            if (result && result[evalError])
	            {
	                cmd = api ?
	                    "with("+api+"){ "+expr+" }" :
	                    expr;
	                
	                result = this.eval(cmd);

	            }
	        }
	        else
	        {
	            // try to execute the command using a "return" statement in the evaluation closure.
	            cmd = api ?
	                // with API and context, trying to get the return value
	                "(function(arguments){ with(" + api + "){ return (" + 
	                    expr + 
	                ") } }).call(" + context + ",undefined)"
	                :
	                // with context only, trying to get the return value
	                "(function(arguments){ return (" +
	                    expr +
	                ") }).call(" +context + ",undefined)";
	            
	            result = this.eval(cmd);
	            
	            // if it results in error, then try it without the "return" statement 
	            if (result && result[evalError])
	            {
	                cmd = api ?
	                    // with API and context, no return value
	                    "(function(arguments){ with(" + api + "){ " +
	                        expr + 
	                    " } }).call(" + context + ",undefined)"
	                    :
	                    // with context only, no return value
	                    "(function(arguments){ " + 
	                        expr + 
	                    " }).call(" + context + ",undefined)";
	                    
	                result = this.eval(cmd);
	            }
	        }
	        
	        if (result && result[evalError])
	        {
	            var msg = result.name ? (result.name + ": ") : "";
	            msg += result.message || result;
	            
	            if (errorHandler)
	                result = errorHandler(msg)
	            else
	                result = msg;
	        }
	        
	        return result;
	    }
	};

	Adapt.once('adapt:initialize', function() {
		var str = 'Version of Adapt core detected: '+getAdaptCoreVersion();
		var horz = getHorzLine();

		console.log(horz+'\nVersion of Adapt core detected: '+getAdaptCoreVersion()+'\n'+horz);

		function getHorzLine() {
			for (var s='', i=0, c=str.length; i<c; i++) s+='*';
			return s;
		}
	});

	Adapt.once('adapt:initialize devtools:enable', function() {
		if (!Adapt.devtools.get('_isEnabled')) return;

		$(window).on("keypress", onKeypress);
		$(window).on("mousedown", onMouseDown);
		$(window).on("mouseup", onMouseUp);

		// useful for command-line debugging
		if (!window.a) window.a = Adapt;
	});

	Utils.initialize();

	return Utils;
});
